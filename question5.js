function start() {
    var canvas = document.getElementById("textCan");
    var context = canvas.getContext("2d");
    context.shadowBlur = 6;
    context.shadowOffsetX = 2;
    context.shadowOffsetY=5;
    context.shadowColor = "grey";
    context.fillStyle = "red";
    context.font = "italic 24px serif";
    context.textBaseline = "top";
    context.fillText ("HTML5 Canvas", 0, 0);
}
window.addEventListener("load", start, false);