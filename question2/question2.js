var zeroCell;
var table;
window.addEventListener("load", myFunction, false);

function myFunction() {
    var data;
    var randoNumbers = randomNumbers(true);
    table = document.createElement('table');
    for (var i = 0; i < 4; i++) {
        var row = document.createElement('tr');
        for (var x = 0; x < 4; x++) {
            data = document.createElement('td');
            data.addEventListener('click', function() {
                checkNodes(this)
            }, false);
            var cellValue = randoNumbers.pop();
            var idName = i + "-" + x;
            data.id = idName;
            var picture = document.createElement('img');
            picture.src = "HogsBackFalls" + cellValue + ".jpeg";
            picture.width = 150;
            picture.height = 150;
            data.appendChild(picture);
            row.appendChild(data);
            if (cellValue == 8) {
                zeroCell = data;
            }
        }
        table.appendChild(row);
    }
    document.body.appendChild(table);
}

function checkNodes(node) {
    var index = node.cellIndex;
    var above = null;
    var bellow = null;
    //Find adjacent cells.
    var previous = node.previousSibling;
    var next = node.nextSibling;
    //find rows and cells above and bellow clicked cell.
    var rowNode = node.parentNode;
    var rowAbove = rowNode.previousSibling;
    var rowBellow = rowNode.nextSibling;
    if (rowAbove != null) {
        above = rowAbove.cells[index];
    }
    if (rowBellow != null) {
        bellow = rowBellow.cells[index];
    }
    if (previous != null && previous.isSameNode(zeroCell)) {
        switchCells(node, previous);
    } else if (next != null && next.isSameNode(zeroCell)) {
        switchCells(node, next);
    } else if (above != null && above.isSameNode(zeroCell)) {
        switchCells(node, above);
    } else if (bellow != null && bellow.isSameNode(zeroCell)) {
        switchCells(node, bellow);
    }
    if (checkStatus()) {
        if (confirm("Congrats! Do you want to play again? ")) {
            location.reload();
        }
    }
}

function randomNumbers(shuffle) {
    var arr = [];
    for (var i = 15, x = 0; i >= 0; i--) {
        arr[x] = i;
        x++
    }
    if (shuffle) {
        for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
    }
    return arr;
}

function switchCells(clickedCell, blankCell) {
    var temp = blankCell.innerHTML;
    blankCell.innerHTML = clickedCell.innerHTML;
    clickedCell.innerHTML = temp;
    zeroCell = clickedCell;
}

function checkStatus() {
    var pictureNumber = 0
    for (var i = 0, row; row = table.rows[i]; i++) {
        for (var j = 0, col; col = row.cells[j]; j++) {
            var source = col.childNodes[0].src;
            var relativeSource = source.substring(source.lastIndexOf("/") + 1);
            if (relativeSource != "HogsBackFalls" + pictureNumber++ + ".jpeg") {
                return false
            }
        }
    }
    return true;
}