<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-system="about:legacy-compat"/>
	
	<xsl:template match="/">
		<html>
			<xsl:apply-templates/>
		</html>
	</xsl:template>
	<xsl:template match="cookie">
		<head>
			<meta charset="utf-8"/>
			<title>Cookie</title>
		</head>

		<body>
			<h1>Nutritional Value</h1>
			<table>
				<xsl:for-each select="nutritionalFacts/nutritionalFact">
					<tr>
						<td>
							<xsl:value-of select="@type"/>
						</td>
						<td>
							<xsl:value-of select="text()"/>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</body>
	</xsl:template>
</xsl:stylesheet>