var mouseDown = false;

function updatePicturePostion(thePicture, theEvent) {
    if (mouseDown) {
    	var xMove = theEvent.clientX - (thePicture.width/2);
    	var yMove = theEvent.clientY - (thePicture.height/2)
        thePicture.style.top = yMove + "px";
        thePicture.style.left = xMove + "px";
    }
}

function setMouseDown() {
    mouseDown = true;
}

function setMouseUp() {
    mouseDown = false;
}

function setup() {
    var pic = document.getElementById('hogsPic');
    pic.addEventListener("mousemove", function() {
        updatePicturePostion(this, event)
    }, false);
    pic.addEventListener("mousedown", setMouseDown, false);
    pic.addEventListener("mouseup", setMouseUp, false);
    //Just incase picture movement can't keep up with the mouse, make user click again.
    pic.addEventListener("mouseout", setMouseUp,false);

}
window.addEventListener("load", setup, false);