function start() {
    var canvas = document.getElementById("textCan");
    var context = canvas.getContext("2d");
    var gradient = context.createLinearGradient(0, 0, 0, 200);
    gradient.addColorStop(0, "lightSteelBlue");
    gradient.addColorStop(0.5, "red");
    gradient.addColorStop(1, "blue");
    context.beginPath();
    context.moveTo(10, 13);
    context.lineTo(20, 40);
    context.lineTo(40, 50);
    context.lineTo(100, 60);
    context.lineTo(120, 20);
    context.closePath();
    context.fillStyle = gradient;
    context.fill();
    context.lineWidth = 1;
    context.lineJoin = "miter";
    context.strokeStyle = "black";
    context.stroke();
}
window.addEventListener("load", start, false);